try:
    from Scenario2.datatypes import FloatType
    from Scenario2.interpolators import FloatScalarInterpolator
    from Scenario2.director import Director
    from Scenario2.actor import Actor, CustomActor
    from Scenario2.keyframes import ValueObject, ValueGenerator, KF, Interval
except:
    pass

class foo:

    def __init__(self):
        self.a= 2.0

    def set(self, value):
        self.a = value

    def get(self):
        return self.a
    

def test_001ActorInDirector():

    d = Director()
   
    f = foo()

    # create an actor
    actor = Actor('test', f, 0.5, FloatType, FloatScalarInterpolator)
    assert len(actor.actions.keyframes) == 1
    assert actor.actions.keyframes[0].getValue() == 0.5

    # create a director
    actor = d.addActor(actor)
    assert actor._maa() == d

    # run the animation
    print "Run keyframe 0"
    d.run()


def test_002CustomActorInDirector():

    d = Director()

    f = foo()

    # create an actor
    actor = CustomActor('test', f, 0.5, FloatType, FloatScalarInterpolator,
                        setMethod='set', getFunction=(f.get, (), {}))
    assert len(actor.actions.keyframes) == 1
    assert actor.actions.keyframes[0].getValue() == 0.5

    # add actor to director
    actor = d.addActor(actor)
    
    # run the animation
    print "Run keyframe 0"
    d.run()


    # add Interval
    kf1 = KF( 10, 25 )
    kf2 = KF( 20, 35 )
    i1 = Interval(kf1, kf2, )
    actor.addIntervals( [i1] )
    assert d.endFrame == 20
    d.run()


## actor.setKeyframe( 10, 10.5)
## print "Run keyframe 0 and 1"
## d.run()

## # add a keyframe at 15
## actor.setKeyframe( 20, 10.5)
## actor.setKeyframe( 25, 5.5)
## assert len(actor.valueGenerators)==4
## assert len(actor.keyframes.kfs)==4
## print "Run keyframe 0,1,2,3"
## d.run()

## # stop interpolation between keyframe 1 and 2 (i.e interval 1)
## actor.valueGenerators[1].configure(active = False)
## #actor.setValueGenerator( None, segment=1)
## print "Run keyframe 0,1 and 2,3"
## d.run()

