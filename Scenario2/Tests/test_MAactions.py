from Scenario2.keyframes import ValueObject, ValueGenerator,KF, Interval
from Scenario2.interpolators import FloatScalarInterpolator
from Scenario2.actions import Actions
from Scenario2.actor import Actor
from Scenario2.multipleActorsActions import MultipleActorsActions


def test_001MAActionsConstructor():
    """
    test MultipleActorActions object contructor
    """
    # create object with no value
    maa = MultipleActorsActions()
    assert maa.name == 'MAactions'
    assert len(maa.actors) == 0


def dtest_002MAActionsAddActionsAt():
    """
    test adding actions to MultipleActorsActions object
    """
    # create object with no value
    maa = MultipleActorsActions()

    # create an action to be inserted
    a1 = Actions()
    kf1 = KF(0,0)
    kf2 = KF(10,5.)
    a1.addInterval(Interval(kf1, kf2))

    # create an actor
    actor1 = Actor(None, 'actor1')
    
    # add the action for this actor
    maa.AddActions( actor1, a1 )
    assert maa.actions.has_key(actor1)
    assert len(maa.actions) == 1
    #assert maa.actions[actor1][0] == a1
    
    # add a second action over empty space
    #a2 = a1.copyAt( 40 )
    maa.AddActionsAt( actor1, a1, 40)
    assert len(maa.actions[actor1]) == 2
    #assert maa.actions[actor1][0] == a1
    #assert maa.actions[actor1][1] == a2

    # check that adding third actor at 20 will insert in second position
    a3 = a1.copyAt( 20 )
    maa.AddActions( actor1, a3 )
    assert len(maa.actions[actor1]) == 3
    assert maa.actions[actor1][0] == a1
    assert maa.actions[actor1][1] == a3
    assert maa.actions[actor1][2] == a2
    
    # check that adding actor that overlaps fails
    a4 = a1.copyAt( 5 )
    val = maa.AddActions( actor1, a4 )
    assert val==False
    assert len(maa.actions[actor1]) == 3
    assert maa.actions[actor1][0] == a1
    assert maa.actions[actor1][1] == a3
    assert maa.actions[actor1][2] == a2

    a4 = a1.copyAt( 25 )
    val = maa.AddActions( actor1, a4 )
    assert val==False
    assert len(maa.actions[actor1]) == 3
    assert maa.actions[actor1][0] == a1
    assert maa.actions[actor1][1] == a3
    assert maa.actions[actor1][2] == a2
    
