from Scenario2.actor import Actor, CustomActor
from Scenario2.keyframes import ValueObject, ValueGenerator,KF, Interval, \
     AutoCurrentValueObject, KFAutoCurrentValue
from Scenario2.actions import Actions

class Object:

    def __init__(self):
        self.value = 0

    def get(self):
        return self.value

    def set(self, value):
        self.value = value


def getter(obj):
    return obj.get()


def test_001ActorConstructors():
    obj = Object()
    a = Actor('ObjectActor', obj)

    ca = CustomActor('CustomObjectActor', obj, setMethod='set',
                     getFunction=(getter, (), {}))
    
    

   
def test_008AutoCurrentValueObject():
    """
    test that this class return the current value of the associated object
    """
    class Object:
        def __init__(self, value):
            self.value = value
        def get(self):
            return self.value
        def set(self, value):
            self.value = value

    obj = Object(5)

    actor = CustomActor('obj', obj, setMethod='set',
                        getFunction=(obj.get, (), {}))

    autoValue = AutoCurrentValueObject(actor)
    kf1 = KF(0, autoValue)
    assert isinstance(kf1.valueObject, AutoCurrentValueObject)
    assert kf1.getValue() == 5

    # change the object's value
    obj.set(8)

    # now the keyframe has to return the obejct's currnet value 8
    assert kf1.getValue() == 8


def test_008AutoCurrentValueObject():
    """
    test that this class return the current value of the associated object
    """
    class Object:
        def __init__(self, value):
            self.value = value
        def get(self):
            return self.value
        def set(self, value):
            self.value = value

    obj = Object(5)

    actor = CustomActor('obj', obj, setMethod='set',
                        getFunction=(obj.get, (), {}))

    kf1 = KFAutoCurrentValue(0, actor)
    assert isinstance(kf1.valueObject, AutoCurrentValueObject)
    assert kf1.getValue() == 5

    # change the object's value
    obj.set(8)

    # now the keyframe has to return the obejct's currnet value 8
    assert kf1.getValue() == 8


def test_008intervalWithKFAutoCurrentValue():
    """
    Test KFAutoCurrentValue in interval.
    """
    class Object:
        def __init__(self, value):
            self.value = value
        def get(self):
            return self.value
        def set(self, value):
            self.value = value

    obj = Object(5.)
    actor = CustomActor('obj', obj, setMethod='set',
                        getFunction=(obj.get, (), {}))
    kf1 = KFAutoCurrentValue(0, actor)
   
    kf2 = KF(10, 10.)
    valGen = ValueGenerator()
    i1 = Interval(kf1, kf2, valGen)

    from Scenario2.interpolators import FloatScalarInterpolator
    gen = FloatScalarInterpolator(None, None)
    i1.setValGenGenerator(gen)

    assert gen.firstVal == 5.
    assert gen.lastVal == 10.

    obj.set(8.)

    assert kf1.getValue() == 8.
    # after gettignthe value the interval using kf1 should
    # have updated first and last values
    assert gen.firstVal == 8.
   
