
from distutils.core import setup
from distutils.command.sdist import sdist
from distutils.command.install_data import install_data
from distutils.command.build_scripts import build_scripts
import os
from string import find

########################################################################
# Had to overwrite the prunrefile_list method of sdist to not
# remove automatically the RCS/CVS directory from the distribution.
########################################################################

class modified_sdist(sdist):
    def prune_file_list(self):
        """
        Prune off branches that might slip into the file list as created
        by 'read_template()', but really don't belong there:
          * the build tree (typically 'build')
          * the release tree itself (only an issue if we ran 'sdist
            previously with --keep-temp, or it aborted)
        """
        build = self.get_finalized_command('build')
        base_dir = self.distribution.get_fullname()
        self.filelist.exclude_pattern(None, prefix=build.build_base)
        self.filelist.exclude_pattern(None, prefix=base_dir)

class modified_install_data(install_data):

    def run(self):
        install_cmd = self.get_finalized_command('install')
        self.install_dir = getattr(install_cmd, 'install_lib')
        return install_data.run(self)


########################################################################

# list of the python packages to be included in this distribution.
# sdist doesn't go recursively into subpackages so they need to be
# explicitaly listed.
# From these packages only the python modules will be taken
packages = ['Scenario2', 'Scenario2.Icons', 'Scenario2.gui',
            'Scenario2.gui.Tk','Scenario2.Tests',
            ]


# list of the files that are not python packages but are included in the
# distribution and need to be installed at the proper place  by distutils.
# The list in MANIFEST.in lists is needed for including those files in
# the distribution, data_files in setup.py is needed to install them
# at the right place.
data_files = []

def getDataFiles(file_list, directory, names):
    fs = []
    for name in names:
        ext = os.path.splitext(name)[1]
        #print directory, name, ext, len(ext)
        if ext !=".py" and ext !=".pyc":
            fullname = os.path.join(directory,name)
            if not os.path.isdir(fullname):
                fs.append(fullname)
    if len(fs):
        file_list.append((directory, fs))

os.path.walk("Scenario2", getDataFiles, data_files)


# description of what is going to be included in the distribution and
# installed.
from version import VERSION
setup (name = 'Scenario2',
       version = VERSION,
       description = "A package providing functionality for modifying Python objects over the course of time according to a scenario. A scenario is defined as a set of events taking place at specific times./nThis package requires the discrete events simulation Python package SimPy(see http://simpy.sourceforge.net/)",
       author = 'Molecular Graphics Laboratory',
       author_email = 'sanner@scripps.edu',
       download_url = 'http://mgltools.scripps.edu/downloads',
       packages = packages,
       data_files = data_files,
       cmdclass = {'sdist': modified_sdist,
                   'install_data': modified_install_data,
                   },
       )
